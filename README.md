# elk-import

Instantiate and configure an ELK cluster and load an apache log file.

## Startup 
1. docker-compose build
2. docker-compose up -d

## Access Dashboard
1. Select the menu icon in the upper left corner and click on Dashboard

![](images/to_dashboard.png)

2. Search for "Apache logs" and click on the Apache Logs dashboard

![](images/apache_dashboard.png)

3. To view the dashboard with the imported data replace the ?_g... with the following
  ?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:'2018-01-01T18:47:49.037Z',to:'2018-03-28T18:03:19.893Z'))

![](images/dashboard_default_url.png)

## Notes
- Initial docker-compose file from https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-docker.html
- This is not completely idempotent as the filebeat container continues sending data to kibana after the first run but not after subsequent runs
- Dashboard visualization issues
  - Ratio of error to success is not likely what we're looking for but I don't have any experience configuring dashboards with Kibana
  - Number of requests for each URL and OS/Browser distribution likely needs to be more granular
- My initial pass at this took around 4 hours