#!/bin/bash

function check_for_kib_up() {
  curl -s -XGET http://kib01:5601/status -I
  if curl -s -XGET http://kib01:5601/status -I ; then
    echo "Up"
  else
    echo "Down"
  fi
}

function wait_for_kib_up() {
  echo "waiting"
  kib_status=$(check_for_kib_up)
  echo ${kib_status}
  while [ "${kib_status}" == "Down" ]; do
    echo "Sleeping 15"
    sleep 15
    kib_status=$(check_for_kib_up)
  done
}

function wait_for_kib_ready() {
  sleep 30
  kibana_status=$(curl -s -XGET http://kib01:5601/status -I | grep HTTP | awk '{print $2}')
}

wait_for_kib_up
wait_for_kib_ready

if [ ${kibana_status} -eq 503 ]; then
  wait_for_kib_ready
fi

if  [ ${kibana_status} -eq 200 ]; then
  filebeat_indexes=$(curl -s -X GET "es01:9200/_cat/indices/*?v=true&s=index&pretty" | awk '{print $3}' | grep -c '^filebeat-')
  if [ ${filebeat_indexes} -eq 0 ]; then
    echo "Configuring Kibana and importing logs..."
    wget https://github.com/mintel/sre-interview-assets/raw/master/challenges/challenge_01_docker_elk_apache_logs/WEB_access_log.log.gz
    gunzip WEB_access_log.log.gz
    filebeat setup -E setup.kibana.host=kib01:5601 -E output.elasticsearch.hosts=['http://es01:9200','http://es02:9200','http://es03:9200']
    curl -X POST kib01:5601/api/saved_objects/_import?createNewCopies=true -H "kbn-xsrf: true" --form file=@basic_dashboard.ndjson
    filebeat -e -strict.perms=false -E output.elasticsearch.hosts=['http://es01:9200','http://es02:9200','http://es03:9200']
  else
    echo "Kibana has already been configured.  Exiting."
  fi
fi